import axios, { AxiosResponse } from "axios";
import { env } from "../../environments/environment";


export class RealTimeWheaterApi  {

    


   public async fetchAll() : Promise<{}> { 

    const options = {
        method: 'GET',
        url: env.apiUrl,
        params: {q: env.q},
        headers: {
          'X-RapidAPI-Key': env.XRapidAPIKey,
          'X-RapidAPI-Host': env.XRapidAPIHost
        }
      };

    return  await axios.request(options)
   }
    
}
